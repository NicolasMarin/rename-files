# Rename-Files

A Command line application to rename all files in a directory with the same pattern.

*Read this in other language: [Spanish](README.es.md)


## Prerequisites

To use the application, it is necessary to have the jdk (Java Development Kit) installed, (if you use windows, have the path of the "bin" folder of the jdk added to the "Path" environment variable (For example: C:\Program Files\Java\jdk1.8.0_131\bin)) and to have installed Maven.

Once the repository has been cloned (git clone https://gitlab.com/NicolasMarin/rename-files.git) and positioned in the project (cd rename-files), we compile and package the application (mvn -q -Dmaven.test.skip package).


## Instructions for use

To run the application we write "java -jar target/RenameFiles-1.0-SNAPSHOT.jar 'directory' arg1 arg2 arg3".

To make it work, after "java -jar target/RenameFiles-1.0-SNAPSHOT.jar" we have to pass it at least 2 parameters. The first is the directory with the files to be renamed and then a parameter to configure the rename. These parameters to rename are made up of a sign that represents the action and a number or string of characters that represents how many characters of the file name the action will be applied.

'-n' : means that the following n characters from the file name will be removed.

'=n' : means that the following n characters of the file name will be retained.

'+abcd' : means that the string 'abcd' is added to the file name.

The application can receive multiple combinations of parameters to choose different parts of the name of the files when renaming. For example, if I run "java -jar target/RenameFiles-1.0-SNAPSHOT.jar 'directoryPath' =4 -2 +Name =1" (it works the same if I use the last one = or not) having the following files in directoryPath:

File--1.txt

File--2.mp3

File--3.jpg

I would get as renowned files:

FileName1.txt

FileName2.mp3

FileName3.jpg

Once "java -jar target/RenameFiles-1.0-SNAPSHOT.jar ..." is used, list the files with their original names by console, followed by their new names, and ask if you want to make the changes (enter 'y' to make the changes and 'n' to cancel them).
