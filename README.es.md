# Rename-Files

Una Aplicacion de linea de comandos para renombrar todos los archivos de un directorio con el mismo patron.

*Leer esto en otro Idioma: [Ingles](README.md)


## Pre-requisitos

Para usar la aplicacion es necesario tener instalado el jdk (Java Development Kit), (si usas windows, tener agregado en la variable de entorno "Path" la ruta de la carpeta "bin" del jdk (Por Ejemplo: C:\Program Files\Java\jdk1.8.0_131\bin)) y tener instalado Maven.

Una vez clonado el repositorio (git clone https://gitlab.com/NicolasMarin/rename-files.git) y posicionados en el proyecto (cd rename-files), compilamos y empaquetamos la aplicacion (mvn -q -Dmaven.test.skip package).


## Instrucciones de uso

Para ejecutar la aplicacion escribimos "java -jar target/RenameFiles-1.0-SNAPSHOT.jar 'directory' arg1 arg2 arg3".

Para que funcione, luego de "java -jar target/RenameFiles-1.0-SNAPSHOT.jar" tenemos que pasarle por lo menos 2 parametros. El primero es el directorio con los archivos a renombrar y luego un parametro para configurar el renombrado. Estos parametros para renombrar se conforman de un signo que representa la accion y un numero o cadena de caracteres que representa a cuantos caracteres del nombre de los archivos se va a aplicar la accion.

'-n' : significa que se eliminaran los siguientes n caracteres del nombre del archivo.

'=n' : significa que se conservaran los siguientes n caracteres del nombre del archivo.

'+abcd' : significa que se agregara la cadena 'abcd' al nombre del archivo.

La aplicacion puede recibir multiples combinaciones de parametros para elegir distintas partes del nombre de los archivos al renombrar. Por Ejemplo, si ejecuto "java -jar target/RenameFiles-1.0-SNAPSHOT.jar 'directoryPath' =4 -2 +Name =1" (funciona igual si uso el ultimo = o no ) teniendo en directoryPath los siguientes archivos:

File--1.txt

File--2.mp3

File--3.jpg

Obtendria como archivos renombrados:

FileName1.txt

FileName2.mp3

FileName3.jpg

Una vez usado "java -jar target/RenameFiles-1.0-SNAPSHOT.jar ...", lista por consola los archivos con sus nombres originales, seguidos por sus nombres nuevos, y pregunta si desea realizar los cambios (ingrese 'y' para realizar los cambios y 'n' para cancelarlos).
