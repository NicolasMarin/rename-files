package RenameFiles;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class AnswerAsker {
	
	private final Scanner keyboard;
    private final PrintStream out;
    
    public AnswerAsker() {
    	keyboard = new Scanner(System.in);
        this.out = System.out;
    }
    
    public AnswerAsker(InputStream in, PrintStream out) {
    	keyboard = new Scanner(in);
        this.out = out;
    }

    public String getAnswer(String preMessage, String potsMessage) {
    	out.print(preMessage);
    	String answer = keyboard.nextLine().toUpperCase();
    	keyboard.close();
    	out.print(potsMessage + answer + "\n");
    	return answer;
    }

}
