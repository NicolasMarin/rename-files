package RenameFiles;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RenameFiles {
	
	public static List<File> originalFiles = new ArrayList<File>();
	public static List<File> newFiles = new ArrayList<File>();
	
	public static void main(String[] args) {
		
		if(areValidArguments(args)) {
			
			System.out.println(Alerts.filesRenaming);
			loadFilesList(args);
			handlerAnswer();
		}
	}
	
	private static void loadFilesList(String[] args) {
		File[] dirArrey = new File(args[0]).listFiles();
		for(File file: dirArrey) {
			if(file.isFile()) {
				loadFile(file, args);
			}
		}
	}

	private static void loadFile(File file, String[] args) {
		originalFiles.add(file);
		File newFile = getNewFile(file, args);
		System.out.println(" * " + file.getName() + "     " + newFile.getName());
		newFiles.add(newFile);
	}

	public static File getNewFile(File file, String[] args) {
		String newPath = file.getParent() + File.separator + getNewNameCustom(file.getName(), "", args, 1);
		return new File(newPath);
	}
	
	public static String getNewNameCustom(String originalName, String newName, String[] args, int index) {
		if(index < args.length) {
			char action = args[index].charAt(0);
			
			if(action == '+') {
				String a = args[index].substring(1, args[index].length());
				newName = newName + a;
				newName = getNewNameCustom(originalName, newName, args, index+1);;
			}else {
				int cant = Integer.valueOf(args[index].substring(1, args[index].length()));
				
				if(cant > originalName.length() - 4) {//if the quantity(cant) of the parameter exceeds the extension of the original name, modify only up to the extension
					cant = originalName.length() - 4;
					if(action == '=') {
						newName = newName + originalName.substring(0, cant);
					}
					newName = newName + originalName.substring(cant, originalName.length());
				}else {
					if(action == '=') {
						newName = newName + originalName.substring(0, cant);
					}
					originalName = originalName.substring(cant, originalName.length());
					newName = getNewNameCustom(originalName, newName, args, index+1);
				}
			}
			return newName;
		}else {//add the rest of the characters that are not affected by the parameters
			return newName + originalName;
		}
	}

	private static void handlerAnswer() {
		String answer = new AnswerAsker(System.in, System.out).
				getAnswer(Alerts.GetChangesValidationMessage(), Alerts.answer);
		
		if(answer.equals(Alerts.acceptanceCharacter)) {
			replaceAllFileNames();
		}
	}

	private static void replaceAllFileNames() {
		boolean show = false;
		for(int i = 0; i < originalFiles.size(); i++) {
			show = originalFiles.get(i).renameTo(newFiles.get(i)) || show;
		}
		if(show) {
			System.out.println(Alerts.renamingSuccessful);
		}
	}
	
	public static boolean areValidArguments(String[] args) {
		if(args.length < 2) {//there must be at least 2 parameters, the directory path and a configuration parameter
			System.out.print(Alerts.missingParameters);
			return false;
		}
		if(!new File(args[0]).isDirectory()) {//validated that the first parameter is a valid directory address
			System.out.print(Alerts.invalidDirectory);
			return false;
		}
		if(new File(args[0]).listFiles().length == 0) {
			System.out.print(Alerts.emptyDirectory);
			return false;
		}
		return areValidConfigurationParameters(args);
	}
	
    public static boolean areValidConfigurationParameters(String[] args) {//validated every configuration parameter
    	for(int i = 1; i < args.length; i++) {
			
    		if(args[i].length() == 0) {
    			return false;
    		}
    		if(args[i].length() == 1) {//validated that the parameters have one character per action and at least one for the number
				System.out.print(Alerts.GetInvalidParameterMessage(args[i]));
				return false;
			}
			if(args[i].charAt(0) != '-' && args[i].charAt(0) != '=' && args[i].charAt(0) != '+') {//validated that the first character of the parameter is a valid character ( -, =)
				System.out.print(Alerts.GetNotActionCharacter(args[i]));
				return false;
			}
			if(args[i].charAt(0) != '+' && !Utilities.isNumeric(args[i].substring(1, args[i].length()))) {//validated that after the first character of the parameters the rest are numbers
				System.out.print(Alerts.GetNotNumericString(args[i]));
				return false;
			}
		}
		return true;
	}
}
