package RenameFiles;

public class Alerts {
	
	public static String filesRenaming = "The Archives would be renamed as follows:";
	public static String changesValidation = "Make changes (%s / N)\n";
	public static String acceptanceCharacter = "Y";
	public static String answer = "Answered: ";
	public static String renamingSuccessful = "The files have been renamed successfully";
	
	public static String missingParameters = "Parameters missing\n";
	public static String invalidDirectory = "Invalid Directory\n";
	public static String emptyDirectory = "Empty Directory\n";
	public static String invalidParameter = "Parameter %s is invalid\n";//25
	public static String notActionCharacter = "The first value of the parameter %s has to be - or =\n";
	public static String notNumericString = "After the first value of the parameter %s it has to be a number\n";
	
	public static String GetChangesValidationMessage() {
		return changesValidation.replace("%s", acceptanceCharacter);
	}
	
	public static String GetInvalidParameterMessage(String string) {
		return invalidParameter.replace("%s", string);
	}

	public static String GetNotActionCharacter(String string) {
		return notActionCharacter.replace("%s", string);
	}

	public static String GetNotNumericString(String string) {
		return notNumericString.replace("%s", string);
	}
	
}