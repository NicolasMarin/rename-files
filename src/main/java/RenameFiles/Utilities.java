package RenameFiles;

public class Utilities {
	
	public static boolean isNumeric(String string){
    	try {
    		Integer.parseInt(string);
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}
    }
	
	public static boolean compareExceptTheParameter(String withParameter, String withoutParameter) {
		int parameterLegth = Math.abs(withoutParameter.length() - 2 - withParameter.length());
		int indexApparence = withoutParameter.indexOf("%s");
		
		String withoutParameterTrimmed = withoutParameter.replace("%s", "");
		try {
			String withParameterTrimmed = withParameter.substring(0, indexApparence) + withParameter.substring(indexApparence + parameterLegth, withParameter.length());
			return withoutParameterTrimmed.equals(withParameterTrimmed);
		}catch(StringIndexOutOfBoundsException e) {
			return false;
		}
	}

}
