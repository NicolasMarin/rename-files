package RenameFiles;

import static org.junit.Assert.assertEquals;
import java.io.File;

import org.junit.Test;

public class RenameFilesTest {
	
	public String originalName = "FileName1.mp4";
	public String newName = "", resultName = "";
	public int index = 1;
	
	@Test
	public void getNewNameCustomTest1() {
		String[] args = {"", "=2", "-3", "=4"};
		resultName = RenameFiles.getNewNameCustom(originalName, newName, args, index);
		assertEquals(resultName, "Fiame1.mp4");
	}
	
	@Test
	public void getNewNameCustomTest2() {
		String[] args = {"", "=2"};
		resultName = RenameFiles.getNewNameCustom(originalName, newName, args, index);
		assertEquals(resultName, "FileName1.mp4");
	}
	
	@Test
	public void getNewNameCustomTest3() {
		String[] args = {"", "-4"};
		resultName = RenameFiles.getNewNameCustom(originalName, newName, args, index);
		assertEquals(resultName, "Name1.mp4");
	}
	
	@Test
	public void getNewNameCustomTest4() {
		String[] args = {"", "-4", "=3"};
		resultName = RenameFiles.getNewNameCustom(originalName, newName, args, index);
		assertEquals(resultName, "Name1.mp4");
	}
	
	@Test
	public void getNewNameCustomTest5() {
		String[] args = {"", "=40"};
		resultName = RenameFiles.getNewNameCustom(originalName, newName, args, index);
		assertEquals(resultName, "FileName1.mp4");
	}
	
	@Test
	public void getNewNameCustomTest6() {
		String[] args = {"", "-40"};
		resultName = RenameFiles.getNewNameCustom(originalName, newName, args, index);
		assertEquals(resultName, ".mp4");
	}
	
	@Test
	public void getNewNameCustomTest7() {
		String[] args = {"", "=40", "-3"};
		resultName = RenameFiles.getNewNameCustom(originalName, newName, args, index);
		assertEquals(resultName, "FileName1.mp4");
	}
	
	@Test
	public void getNewNameCustomTest8() {
		String[] args = {"", "-40", "=5"};
		resultName = RenameFiles.getNewNameCustom(originalName, newName, args, index);
		assertEquals(resultName, ".mp4");
	}
	
	@Test
	public void getNewNameCustomTest9() {
		String[] args = {"", "-3", "=40"};
		resultName = RenameFiles.getNewNameCustom(originalName, newName, args, index);
		assertEquals(resultName, "eName1.mp4");
	}
	
	@Test
	public void getNewNameCustomTest10() {
		String[] args = {"", "=5", "-40"};
		resultName = RenameFiles.getNewNameCustom(originalName, newName, args, index);
		assertEquals(resultName, "FileN.mp4");
	}
	
	@Test
	public void getNewFileTest() {
		String[] args = {"", "=5", "-40"};
		String originalPath = System.getProperty("user.dir") + File.separator;
		File originalFile = new File(originalPath + originalName);
		File newFile = RenameFiles.getNewFile(originalFile, args);
		File newFileConfirm = new File(originalPath + "FileN.mp4");
		assertEquals(newFile, newFileConfirm);
	}
}