package RenameFiles;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

public class RenameFilesValidationsTest {
	
	ByteArrayOutputStream outContent;
	
	@Before
	public void before() {
		outContent = new ByteArrayOutputStream(); 
        System.setOut(new PrintStream(outContent));
	}
	
	@Test
	public void areValidArgumentsTestEmptyArray() {
        String[] array = {};
		boolean bool = !RenameFiles.areValidArguments(array) && (outContent.toString().equals(Alerts.missingParameters));
		assertTrue(bool);
	}
	
	@Test
	public void areValidArgumentsTestOneParameterArray() {
		String[] array = {"path"};
		boolean bool = !RenameFiles.areValidArguments(array) && (outContent.toString().equals(Alerts.missingParameters));
		assertTrue(bool);
	}
	
	@Test
	public void areValidArgumentsTestInvalidDirectory() {
		String[] array = {"path", "a"};
		boolean bool = !RenameFiles.areValidArguments(array) && (outContent.toString().equals(Alerts.invalidDirectory));
		assertTrue(bool);
	}
	
	@Test
	public void areValidArgumentsTestEmptyDirectory() {
		File directory = new File(System.getProperty("user.dir") + File.separator + "emptyDirectory");
		if(!directory.exists()) {
			directory.mkdir();
		}
		String[] array = {directory.getAbsolutePath(), "a"};
		boolean bool = !RenameFiles.areValidArguments(array) && (outContent.toString().equals(Alerts.emptyDirectory));
		assertTrue(bool);
	}
	
	@Test
	public void areValidConfigurationParametersTestEmptyParameter() {
		String[] array = {"C:"+File.separator, ""};
		assertFalse(RenameFiles.areValidConfigurationParameters(array));
	}
	
	@Test
	public void areValidConfigurationParametersTestInvalidParameter() {
		String[] array = {"C:"+File.separator, "b"};
		boolean bool = !RenameFiles.areValidConfigurationParameters(array) && Utilities.compareExceptTheParameter(outContent.toString(), Alerts.invalidParameter);
		assertTrue(bool);
	}
	
	@Test
	public void areValidConfigurationParametersTestNotActionCharacter() {
		String[] array = {"C:"+File.separator, "ba"};
		boolean bool = !RenameFiles.areValidConfigurationParameters(array) && Utilities.compareExceptTheParameter(outContent.toString(), Alerts.notActionCharacter);
		assertTrue(bool);
	}
	
	@Test
	public void areValidConfigurationParametersTestNotNumericString1() {
		String[] array = {"C:"+File.separator, "-a"};
		boolean bool = !RenameFiles.areValidConfigurationParameters(array) && Utilities.compareExceptTheParameter(outContent.toString(), Alerts.notNumericString);
		assertTrue(bool);
	}
	
	@Test
	public void areValidConfigurationParametersTestNotNumericString2() {
		String[] array = {"C:"+File.separator, "=ab1"};
		boolean bool = !RenameFiles.areValidConfigurationParameters(array) && Utilities.compareExceptTheParameter(outContent.toString(), Alerts.notNumericString);
		assertTrue(bool);
	}
	
	@Test
	public void areValidConfigurationParametersTestValid() {
		String[] array = {"C:"+File.separator, "-6", "=13", "-4"};
		assertTrue(RenameFiles.areValidConfigurationParameters(array));
	}

}
