package RenameFiles;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class UtilitiesTest {
	
	@Test
	public void isNumericTestTrue() {
		assertTrue(Utilities.isNumeric("10"));
	}
	
	@Test
	public void isNumericTestFalse() {
		assertFalse(Utilities.isNumeric("ab1"));
	}
	
	@Test
	public void compareExceptTheParameterTestTrue1() {
		assertFalse(Utilities.compareExceptTheParameter(Alerts.invalidParameter, Alerts.GetInvalidParameterMessage("argument")));
	}
	
	@Test
	public void compareExceptTheParameterTestTrue2() {
		assertTrue(Utilities.compareExceptTheParameter(Alerts.GetInvalidParameterMessage("argument"), Alerts.invalidParameter));
	}
	
	@Test
	public void compareExceptTheParameterTestFalse1() {
		assertFalse(Utilities.compareExceptTheParameter(Alerts.invalidParameter, "argument"));
	}
	
	@Test
	public void compareExceptTheParameterTestFalse2() {
		assertFalse(Utilities.compareExceptTheParameter("argument", Alerts.invalidParameter));
	}

}
